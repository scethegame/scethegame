#include "CPlayer.h"

CPlayer::CPlayer()
{

}

bool CPlayer::OnLoad(SDL_Renderer* Renderer, const char* File, int Width, int Height, int MaxFrames)
{
    if (CEntity::OnLoad(Renderer, File, Width, Height, MaxFrames) == false) return false;

    return true;
}

void CPlayer::OnLoop()
{
    CEntity::OnLoop();
}

void CPlayer::OnRender(SDL_Renderer* Renderer)
{
    CEntity::OnRender(Renderer);
}

void CPlayer::OnCleanup()
{
    CEntity::OnCleanup();
}

void CPlayer::OnAnimate()
{    
    if (SpeedX != 0 || SpeedY != 0)
    {
        Anim_Control.MaxFrames = MaxFrames;
    }
    else
    {
        Anim_Control.MaxFrames = 0;
    }

    CEntity::OnAnimate();
}

bool CPlayer::OnCollision(CEntity* Entity)
{
    return true;
}

bool CPlayer::OnInteract()
{
    /*
     * Depending on which direction we are facing, look for an entity near us
     * and interact with it.
     *
     * We want to take the direction facing and find if there is anything within
     * 1/4 the TILE_WIDTH or TILE_HEIGHT depending on direction facing, if there
     * is we want to interact with it.
     */
    std::cout << "(Player) Spacebar has been pressed" << std::endl;

    int tX, tY, Wth, Hght;
    
    if  (DirectionFacing == FACING_WEST)
    {
        tX = (int)X - (TILE_WIDTH / 4);
        tY = (int)Y;

        Wth = (int)(TILE_WIDTH / 4);
        Hght = Height;
    }
    else if (DirectionFacing == FACING_EAST)
    {
        tX = (int)X - TILE_WIDTH;
        tY = (int)Y;

        Wth = (int)(TILE_WIDTH / 4);
        Hght = Height;
    }
    else if (DirectionFacing == FACING_NORTH)
    {
        tX = (int)X;
        tY = (int)Y - (TILE_HEIGHT / 4);

        Wth = Width;
        Hght = (int)(TILE_HEIGHT / 4);
    }
    else if (DirectionFacing == FACING_SOUTH)
    {
        tX = (int)X;
        tY = (int)Y + Height;

        Wth = Width;
        Hght = (int)(TILE_HEIGHT / 4);
    }

    CEntity* Entity = nullptr;
    if ((Entity = CEntity::EntityInArea(tX, tY, Wth, Hght)) == nullptr) return false;

    // Call the entity's OnInteract Method
    Entity->OnInteract();

    return true;
}