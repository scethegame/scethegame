#ifndef _DEFINE_H_
#define _DEFINE_H_

#define MAP_WIDTH 20
#define MAP_HEIGHT 20

#define TILE_SIZE 16
#define TILE_WIDTH 16
#define TILE_HEIGHT 16

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

#define WINDOW_TITLE "SDL_Tutorial"

#endif