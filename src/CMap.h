#ifndef _CMAP_H_
#define _CMAP_H_

#include <SDL2/SDL.h>
#include <vector>
#include <iostream>

#include "define.h"

#include "CTile.h"
#include "CTexture.h"

enum
{
    MAP_LAYER_GROUND = 0,
    MAP_LAYER_ITEMS,
    MAP_LAYER_SKY
};

class CMap
{
private:
    std::vector<CTile> TileList;

public:
    SDL_Texture* Texture_Tileset;

public:
    CMap();

    bool OnLoad(const char* File);

    void OnRender(SDL_Renderer* Renderer, int MapX, int MapY);

    CTile* GetTile(int X, int Y);
};

#endif