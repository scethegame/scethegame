#include "CApp.h"

CApp::CApp()
{
    Running = true;

    Window = nullptr;
    Renderer = nullptr;

    Texture_Test = nullptr;
}

int CApp::OnExecute()
{
    if (OnInit() == false) return -1;

    SDL_Event Event;

    // Run game here
    while (Running)
    {
        while (SDL_PollEvent(&Event))
        {
            OnEvent(&Event);
        }

        OnLoop();
        OnRender();
    }

    OnCleanup();

    return 0;
}

bool CApp::OnInit()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) return false;

    if ((Window = SDL_CreateWindow(WINDOW_TITLE,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   WINDOW_WIDTH,
                                   WINDOW_HEIGHT,
                                   SDL_WINDOW_OPENGL)) == nullptr) return false;

    if ((Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED)) == nullptr) return false;

    //if ((Texture_Test = CTexture::OnLoad(Renderer, "assets/imgs/characters/yoshi2.png")) == nullptr) return false;

    // Need to determin if we are starting a new game or continuing a game
    if (CArea::AreaControl.OnLoad(Renderer, "assets/maps/player_selection/player_selection.area") == false) return false;

    // Player
    if (Player.OnLoad(Renderer, "assets/imgs/characters/BoloZolo.png", 32, 32, 3) == false) return false;
    //Player.Y = 70;
    //Player.X = 70;

    CEntity::EntityList.push_back(&Player);

    CCamera::CameraControl.TargetMode = TARGET_MODE_CENTER;
    CCamera::CameraControl.SetTarget(&Player.X, &Player.Y);

    return true;
}

void CApp::OnEvent(SDL_Event* Event)
{
    CEvent::OnEvent(Event);
}

void CApp::OnExit()
{
    Running = false;
}

void CApp::OnLoop()
{
    CFPS::FPSControl.OnLoop();

    for (int i=0; i < CEntity::EntityList.size(); i++)
    {
        if (!CEntity::EntityList[i]) continue;
        CEntity::EntityList[i]->OnLoop();
    }

    // Collision Events
    for (int i=0; i<CEntityCol::EntityColList.size(); i++)
    {
        if (CEntityCol::EntityColList[i].EntityA == nullptr || CEntityCol::EntityColList[i].EntityB == nullptr) continue;

        if (CEntityCol::EntityColList[i].EntityA->OnCollision(CEntityCol::EntityColList[i].EntityB))
        {
            CEntityCol::EntityColList[i].EntityB->OnCollision(CEntityCol::EntityColList[i].EntityA);
        }
    }

    CEntityCol::EntityColList.clear();
}

void CApp::OnRender()
{
    // Clear screen buffer
    SDL_RenderClear(Renderer);

    CArea::AreaControl.OnRender(Renderer, -CCamera::CameraControl.GetX(), -CCamera::CameraControl.GetY());

    for (int i=0; i < CEntity::EntityList.size(); i++)
    {
        if (!CEntity::EntityList[i]) continue;
        CEntity::EntityList[i]->OnRender(Renderer);
    }

    // Display buffer to the screen
    SDL_RenderPresent(Renderer);
}

void CApp::OnCleanup()
{
    CArea::AreaControl.OnCleanup();

    // Destroy Test Texture
    //SDL_DestroyTexture(Texture_Test);

    for (int i=0; i < CEntity::EntityList.size(); i++)
    {
        if (!CEntity::EntityList[i]) continue;
        CEntity::EntityList[i]->OnCleanup();
    }

    CEntity::EntityList.clear();

    // Destroy Renderer
    SDL_DestroyRenderer(Renderer);

    // Destroy the window
    SDL_DestroyWindow(Window);

    // Cleanup
    SDL_Quit();
}

void CApp::OnKeyDown(SDL_Keycode sym, Uint16 mod)
{
    switch (sym)
    {
        case SDLK_LEFT:
        {
            Player.MoveLeft = true;
            break;
        }

        case SDLK_RIGHT:
        {
            Player.MoveRight = true;
            break;
        }

        case SDLK_UP:
        {
            Player.MoveUp = true;
            break;
        }

        case SDLK_DOWN:
        {
            Player.MoveDown = true;
            break;
        }

        case SDLK_SPACE:
        {
            Player.OnInteract();
            break;
        }

        default:
        {

        }
    }
}

void CApp::OnKeyUp(SDL_Keycode sym, Uint16 mod)
{
    switch (sym)
    {
        case SDLK_LEFT:
        {
            Player.MoveLeft = false;
            break;
        }

        case SDLK_RIGHT:
        {
            Player.MoveRight = false;
            break;
        }

        case SDLK_UP:
        {
            Player.MoveUp = false;
            break;
        }

        case SDLK_DOWN:
        {
            Player.MoveDown = false;
            break;
        }

        default:
        {
            
        }
    }
}