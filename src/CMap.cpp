#include "CMap.h"

CMap::CMap()
{
    Texture_Tileset = nullptr;
}

bool CMap::OnLoad(const char* File)
{
    TileList.clear();

    FILE* FileHandle = fopen(File, "r");

    if (FileHandle == nullptr) return false;

    for (int Y=0; Y < MAP_HEIGHT; Y++)
    {
        for (int X=0; X < MAP_WIDTH; X++)
        {
            CTile tempTile;

            fscanf(FileHandle, "%d:%d ", &tempTile.TileID, &tempTile.TypeID);

            TileList.push_back(tempTile);
        }
        fscanf(FileHandle, "\n");
    }

    fclose(FileHandle);

    return true;
}

void CMap::OnRender(SDL_Renderer* Renderer, int MapX, int MapY)
{
    if (Texture_Tileset == nullptr) return;

    int Texture_Tileset_W;
    int Texture_Tileset_H;
    SDL_QueryTexture(Texture_Tileset, nullptr, nullptr, &Texture_Tileset_W, &Texture_Tileset_H);

    int TilesetWidth = Texture_Tileset_W / TILE_WIDTH;
    int TilesetHeight = Texture_Tileset_H / TILE_HEIGHT;

    int ID = 0;

    for (int Y=0; Y < MAP_HEIGHT; Y++)
    {
        for (int X=0; X < MAP_WIDTH; X++)
        {
            if (TileList[ID].TypeID == TILE_TYPE_NONE)
            {
                ID++;
                continue;
            }

            int tX = MapX + (X * TILE_WIDTH);
            int tY = MapY + (Y * TILE_HEIGHT);

            int TilesetX = (TileList[ID].TileID % TilesetWidth) * TILE_WIDTH;
            // This looks weird but it is correct
            int TilesetY = (TileList[ID].TileID / TilesetWidth) * TILE_WIDTH;

            // Draw tile to Renderer
            //CTexture::OnDraw(Renderer, Texture_Tileset, tX, tY, TilesetX, TilesetY, TILE_WIDTH, TILE_HEIGHT);
            CTexture::OnDrawTile(Renderer, Texture_Tileset, tX, tY, TilesetX, TilesetY);

            ID++;
        }
    }
}

CTile* CMap::GetTile(int X, int Y)
{
    int ID = 0;

    ID = X / TILE_WIDTH;
    ID = ID + (MAP_WIDTH * (Y / TILE_HEIGHT));

    if (ID < 0 || ID >= TileList.size())return nullptr;

    return &TileList[ID];
}