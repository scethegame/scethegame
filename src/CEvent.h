#ifndef _CEVENT_H_
#define _CEVENT_H_

#include <SDL2/SDL.h>

class CEvent
{
public:
    CEvent();

    virtual ~CEvent();

    virtual void OnEvent(SDL_Event* Event);
    virtual void OnInputFocus();
    virtual void OnInputBlur();
    //virtual void OnKeyDown(SDL_Keysym sym, SDL_Keymod mod, Uint16 unicode);
    //virtual void OnKeyUp(SDL_Keysym sym, SDL_Keymod mod, Uint16 unicode);
    virtual void OnKeyDown(SDL_Keycode sym, Uint16 mod);
    virtual void OnKeyUp(SDL_Keycode sym, Uint16 mod);
    virtual void OnMouseFocus();
    virtual void OnMouseBlur();
    virtual void OnMouseMove(int mX, int mY, int relX, int relY, bool Left, bool Right, bool Middle);
    virtual void OnMouseWheel(bool Up, bool Down);
    virtual void OnLButtonDown(int mX, int mY);
    virtual void OnLButtonUp(int mX, int mY);
    virtual void OnRButtonDown(int mX, int mY);
    virtual void OnRButtonUp(int mX, int mY);
    virtual void OnMButtonDown(int mX, int mY);
    virtual void OnMButtonUp(int mX, int mY);
    virtual void OnJoyAxis(Uint8 Which, Uint8 Axis, Sint16 Value);
    virtual void OnJoyButtonDown(Uint8 Which, Uint8 Button);
    virtual void OnJoyButtonUp(Uint8 Which, Uint8 Button);
    virtual void OnJoyHat(Uint8 Which, Uint8 Hat, Uint8 Value);
    virtual void OnJoyBall(Uint8 Which, Uint8 Ball, Sint16 XRel, Sint16 YRel);
    virtual void OnMinimize();
    virtual void OnRestore();
    virtual void OnResize(int W, int H);
    virtual void OnExpose();
    //virtual void OnClose();
    virtual void OnExit();
    virtual void OnUser(Uint8 Type, int Code, void* Data1, void* Data2);
};

#endif