#ifndef _CTEXTURE_H_
#define _CTEXTURE_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "define.h"

class CTexture
{
public:
    CTexture();

    static SDL_Texture* OnLoad(SDL_Renderer* Renderer, const char* File);

    static bool OnDraw(SDL_Renderer* Renderer, SDL_Texture* Texture, int X, int Y);
    static bool OnDraw(SDL_Renderer* Renderer, SDL_Texture* Texture, int X, int Y, int X2, int Y2, int W, int H);

    static bool OnDrawTile(SDL_Renderer* Renderer, SDL_Texture* Texture, int X, int Y, int X2, int Y2);

    static bool Transparent(SDL_Texture* Texture, int R, int G, int B);
};

#endif