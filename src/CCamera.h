#ifndef _CCAMERA_H_
#define _CCAMERA_H_

#include <SDL2/SDL.h>

#include "define.h"

enum
{
    TARGET_MODE_NORMAL = 0,
    TARGET_MODE_CENTER
};

class CCamera
{
private:
    int X;
    int Y;
    float* TargetX;
    float* TargetY;

public:
    static CCamera CameraControl;
    int TargetMode;

public:
    CCamera();

    int GetX();
    int GetY();

    void OnMove(int MoveX, int MoveY);
    void SetPos(int X, int Y);
    void SetTarget(float* X, float* Y);
};

#endif