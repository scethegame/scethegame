#ifndef _CPLAYER_H_
#define _CPLAYER_H_

#include "CEntity.h"

class CPlayer : public CEntity
{
public:
    CPlayer();

    bool OnLoad(SDL_Renderer* Renderer, const char* File, int Width, int Height, int MaxFrames);

    void OnLoop();
    void OnRender(SDL_Renderer* Renderer);
    void OnCleanup();
    void OnAnimate();
    bool OnCollision(CEntity* Entity);
    bool OnInteract();
};

#endif