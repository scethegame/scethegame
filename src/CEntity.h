#ifndef _CENTITY_H_
#define _CENTITY_H_

#include <vector>

#include "CArea.h"
#include "CAnimation.h"
#include "CCamera.h"
#include "CFPS.h"
#include "CTexture.h"

enum
{
    ENTITY_TYPE_GENERIC = 0,
    ENTITY_TYPE_PLAYER
};

enum
{
    ENTITY_FLAG_NONE = 0,
    ENTITY_FLAG_GRAVITY = 0x00000001,
    ENTITY_FLAG_GHOST = 0x00000002,
    ENTITY_FLAG_MAPONLY = 0x00000004
};

enum
{
    FACING_SOUTH = 0,
    FACING_WEST,
    FACING_EAST,
    FACING_NORTH
};

class CEntity
{
protected:
    CAnimation Anim_Control;
    SDL_Texture* Texture_Entity;

    float SpeedX;
    float SpeedY;

    float AccelX;
    float AccelY;

    int CurrentFrameCol;
    int CurrentFrameRow;

    int DirectionFacing;

    int Col_X;
    int Col_Y;
    int Col_Width;
    int Col_Height;

public:
    static std::vector<CEntity*> EntityList;

    float X;
    float Y;

    int Width;
    int Height;

    int MaxFrames;

    bool MoveLeft;
    bool MoveRight;
    bool MoveUp;
    bool MoveDown;

    float MaxSpeedX;
    float MaxSpeedY;

    int Type;

    bool Dead;
    int Flags;

private:
    bool PosValid(int NewX, int NewY);
    bool PosValidTile(CTile* Tile);
    bool PosValidEntity(CEntity* Entity, int NewX, int NewY);

public:
    CEntity();

    virtual ~CEntity();

    virtual bool OnLoad(SDL_Renderer* Renderer, const char* File, int Width, int Height, int MaxFrames);

    bool Collides(int oX, int oY, int oW, int oH);
    CEntity* EntityInArea(int oX, int oY, int oW, int oH);

    virtual void OnLoop();
    virtual void OnRender(SDL_Renderer* Renderer);
    virtual void OnCleanup();
    virtual void OnAnimate();
    virtual bool OnCollision(CEntity* Entity);
    virtual bool OnInteract();

    void OnMove(float MoveX, float MoveY);
    void StopMove();
};

class CEntityCol
{
public:
    static std::vector<CEntityCol> EntityColList;

    CEntity* EntityA;
    CEntity* EntityB;

public:
    CEntityCol();
};

#endif