#include "CAnimation.h"

CAnimation::CAnimation()
{
    CurrentFrame = 0;
    MaxFrames = 0;
    FrameInc = 1;

    FrameRate = 100; // Milliseconds
    OldTime = 0;

    Oscillate = false;
}

int CAnimation::GetCurrentFrame()
{
    return CurrentFrame;
}

void CAnimation::SetFrameRate(int Rate)
{
    FrameRate = Rate;
}

void CAnimation::SetCurrentFrame(int Frame)
{
    if (Frame < 0 || Frame >= MaxFrames) return;

    CurrentFrame = Frame;
}

void CAnimation::OnAnimate()
{
    if (OldTime + FrameRate > SDL_GetTicks()) return;

    OldTime = SDL_GetTicks();

    CurrentFrame += FrameInc;

    if (Oscillate)
    {
        if (FrameInc > 0)
        {
            if (CurrentFrame >= MaxFrames)
            {
                FrameInc = -FrameInc;
            }
        }
        else
        {
            if (CurrentFrame <= 0)
            {
                CurrentFrame = -FrameInc;
            }
        }
    }
    else
    {
        if (CurrentFrame >= MaxFrames)
        {
            CurrentFrame = 0;
        }
    }
}