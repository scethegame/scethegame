#include "CCamera.h"

CCamera CCamera::CameraControl;

CCamera::CCamera()
{
    X = Y = 0;

    TargetX = TargetY = nullptr;

    TargetMode = TARGET_MODE_NORMAL;
}

int CCamera::GetX()
{
    if (TargetX != nullptr)
    {
        if (TargetMode == TARGET_MODE_CENTER)
        {
            return *TargetX - (WINDOW_WIDTH / 2);
        }
    }

    return X;
}

int CCamera::GetY()
{
    if (TargetY != nullptr)
    {
        if (TargetMode == TARGET_MODE_CENTER)
        {
            return *TargetY - (WINDOW_HEIGHT / 2);
        }
    }

    return Y;
}

void CCamera::OnMove(int MoveX, int MoveY)
{
    X += MoveX;
    Y += MoveY;
}

void CCamera::SetPos(int X, int Y)
{
    this->X = X;
    this->Y = Y;
}

void CCamera::SetTarget(float* X, float* Y)
{
    TargetX = X;
    TargetY = Y;
}