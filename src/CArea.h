#ifndef _CAREA_H_
#define _CAREA_H_

#include "CMap.h"

class CArea
{
private:
    int AreaSize;
    SDL_Texture* Texture_Tileset;

public:
    static CArea AreaControl;
    std::vector<CMap> MapList;

public:
    CArea();

    bool OnLoad(SDL_Renderer* Renderer, const char* File);

    void OnRender(SDL_Renderer* Renderer, int CameraX, int CameraY);
    void OnCleanup();

    CMap* GetMap(int X, int Y);
    CTile* GetTile(int X, int Y);
};

#endif