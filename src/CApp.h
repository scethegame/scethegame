#ifndef _CAAP_H_
#define _CAPP_H_

#include <SDL.h>

#include "define.h"

#include "CAnimation.h"
#include "CArea.h"
#include "CCamera.h"
#include "CEvent.h"
#include "CTexture.h"
#include "CPlayer.h"

class CApp : public CEvent
{
private:
    bool Running;

    SDL_Window* Window;
    SDL_Renderer* Renderer;

    SDL_Texture* Texture_Test;

    CAnimation Anim_Character;

    CPlayer Player;

public:
    CApp();

    int OnExecute();

    bool OnInit();

    void OnEvent(SDL_Event* Event);
    void OnExit();
    void OnLoop();
    void OnRender();
    void OnCleanup();

    void OnKeyDown(SDL_Keycode sym, Uint16 mod);
    void OnKeyUp(SDL_Keycode sym, Uint16 mod);
};

#endif