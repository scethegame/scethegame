#include <iostream>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "define.h"
#include "CApp.h"

int main(int argc, char **argv)
{
    CApp TheApp;

	return TheApp.OnExecute();
}