#include "CTexture.h"

CTexture::CTexture()
{

}

SDL_Texture* CTexture::OnLoad(SDL_Renderer* Renderer, const char* File)
{
    SDL_Surface* Surf_Temp = nullptr;
    SDL_Texture* Texture_Return = nullptr;

    if ((Surf_Temp = IMG_Load(File)) == nullptr) return nullptr;

    Texture_Return = SDL_CreateTextureFromSurface(Renderer, Surf_Temp);
    SDL_FreeSurface(Surf_Temp);

    return Texture_Return;
}

bool CTexture::OnDraw(SDL_Renderer* Renderer, SDL_Texture* Texture, int X, int Y)
{
    if (Renderer == nullptr || Texture == nullptr) return false;

    SDL_Rect DestR;

    DestR.x = X;
    DestR.y = Y;

    int TextureW;
    int TextureH;
    SDL_QueryTexture(Texture, nullptr, nullptr, &TextureW, &TextureH);

    DestR.h = TextureH;
    DestR.w = TextureW;

    SDL_RenderCopy(Renderer, Texture, nullptr, &DestR);

    return true;
}

bool CTexture::OnDraw(SDL_Renderer* Renderer, SDL_Texture* Texture, int X, int Y, int X2, int Y2, int W, int H)
{
    if (Renderer == nullptr || Texture == nullptr) return false;

    SDL_Rect DestR;

    DestR.x = X;
    DestR.y = Y;
    DestR.w = W;
    DestR.h = H;

    SDL_Rect SrcR;

    SrcR.x = X2;
    SrcR.y = Y2;
    SrcR.w = W;
    SrcR.h = H;

    SDL_RenderCopy(Renderer, Texture, &SrcR, &DestR);

    return true;
}

bool CTexture::OnDrawTile(SDL_Renderer* Renderer, SDL_Texture* Texture, int X, int Y, int X2, int Y2)
{
    if (Renderer == nullptr || Texture == nullptr) return false;

    // Source of image from Tilesheet
    SDL_Rect SrcR;
    SrcR.x = X2;
    SrcR.y = Y2;
    SrcR.w = TILE_WIDTH;
    SrcR.h = TILE_HEIGHT;

    // Where to draw this on the screen
    SDL_Rect DestR;
    DestR.x = X;
    DestR.y = Y;
    DestR.w = TILE_WIDTH;
    DestR.h = TILE_HEIGHT;

    SDL_RenderCopy(Renderer, Texture, &SrcR, &DestR);

    return true;
}

bool CTexture::Transparent(SDL_Texture* Texture, int R, int G, int B)
{
    //SDL_SetTextureAlphaMod(Texture, );
}