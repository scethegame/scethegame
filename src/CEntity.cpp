#include "CEntity.h"

std::vector<CEntity*> CEntity::EntityList;

CEntity::CEntity()
{
    Texture_Entity = nullptr;

    X = 0;
    Y = 0;

    Width = 0;
    Height = 0;

    MaxFrames = 0;

    MoveLeft = false;
    MoveRight = false;
    MoveUp = false;
    MoveDown = false;

    Type = ENTITY_TYPE_GENERIC;

    Dead = false;
    Flags = ENTITY_FLAG_NONE;

    SpeedX = 0;
    SpeedY = 0;

    AccelX = 0;
    AccelY = 0;

    MaxSpeedX = 5;
    MaxSpeedY = 5;

    CurrentFrameCol = 0;
    CurrentFrameRow = 0;

    DirectionFacing = FACING_SOUTH;

    Col_X = 0;
    Col_Y = 0;

    Col_Width = 0;
    Col_Height = 0;
}

CEntity::~CEntity()
{

}

bool CEntity::OnLoad(SDL_Renderer* Renderer, const char* File, int Width, int Height, int MaxFrames)
{
    if ((Texture_Entity = CTexture::OnLoad(Renderer, File)) == nullptr) return false;

    CTexture::Transparent(Texture_Entity, 255, 0 ,255);

    this->Width = Width;
    this->Height = Height;
    this->MaxFrames = MaxFrames;

    Anim_Control.MaxFrames = MaxFrames;

    return true;
}

void CEntity::OnLoop()
{
    // Not moving, lets stand still
    if (!MoveLeft && !MoveRight && !MoveUp && !MoveDown) StopMove();

    // Set the Movement
    if (MoveLeft)
    {
        SpeedX = -3;
        SpeedY = 0;
    }
    else if (MoveRight)
    {
        SpeedX = 3;
        SpeedY = 0;
    }
    else if (MoveUp)
    {
        SpeedX = 0;
        SpeedY = -3;
    }
    else if (MoveDown)
    {
        SpeedX = 0;
        SpeedY = 3;
    }

    OnAnimate();
    OnMove(SpeedX, SpeedY);
}

void CEntity::OnRender(SDL_Renderer* Renderer)
{
    if (Texture_Entity == nullptr || Renderer == nullptr) return;

    CTexture::OnDraw(Renderer, 
                     Texture_Entity, 
                     X - CCamera::CameraControl.GetX(), 
                     Y - CCamera::CameraControl.GetY(),
                     (CurrentFrameCol + Anim_Control.GetCurrentFrame()) * Width,
                     CurrentFrameRow * Height,
                     Width,
                     Height);
}

void CEntity::OnCleanup()
{
    SDL_DestroyTexture(Texture_Entity);
    Texture_Entity = nullptr;
}

void CEntity::OnAnimate()
{
    if (MoveLeft)
    {
        //CurrentFrameRow = 1;
        DirectionFacing = FACING_WEST;
    }
    else if (MoveRight)
    {
        //CurrentFrameRow = 2;
        DirectionFacing = FACING_EAST;
    }
    else if (MoveUp)
    {
        //CurrentFrameRow = 3;
        DirectionFacing = FACING_NORTH;
    }
    else if (MoveDown)
    {
        //CurrentFrameRow = 0;
        DirectionFacing = FACING_SOUTH;
    }

    CurrentFrameRow = DirectionFacing;
    Anim_Control.OnAnimate();
}

bool CEntity::OnCollision(CEntity* Entity)
{
    return true;
}

void CEntity::OnMove(float MoveX, float MoveY)
{
    if (MoveX == 0 && MoveY == 0) return;

    double NewX = 0;
    double NewY = 0;

    MoveX *= CFPS::FPSControl.GetSpeedFactor();
    MoveY *= CFPS::FPSControl.GetSpeedFactor();

    if (MoveX != 0)
    {
        if (MoveX > 0)
        {
            NewX = CFPS::FPSControl.GetSpeedFactor();
        }
        else
        {
            NewX = -CFPS::FPSControl.GetSpeedFactor();
        }
    }

    if (MoveY != 0)
    {
        if (MoveY > 0)
        {
            NewY = CFPS::FPSControl.GetSpeedFactor();
        }
        else
        {
            NewY = -CFPS::FPSControl.GetSpeedFactor();
        }
    }

    while (true)
    {
        if (Flags & ENTITY_FLAG_GHOST)
        {
            // We don't care about collisions, but we need to send events to other entities
            PosValid((int)(X + NewX), (int)(Y + NewY));

            X += NewX;
            Y += NewY;
        }
        else
        {
            if (PosValid((int)(X + NewX), (int)(Y)))
            {
                X += NewX;
            }
            else
            {
                SpeedX = 0;
            }

            if (PosValid((int)X, (int)(Y + NewY)))
            {
                Y += NewY;
            }
            else
            {
                SpeedY = 0;
            }
        }

        MoveX += -NewX;
        MoveY += -NewY;

        if (NewX > 0 && MoveX <= 0) NewX = 0;
        if (NewX < 0 && MoveX >= 0) NewX = 0;

        if (NewY > 0 && MoveY <= 0) NewY = 0;
        if (NewY < 0 && MoveY >= 0) NewY = 0;

        if (MoveX == 0) NewX = 0;
        if (MoveY == 0) NewY = 0;

        if (MoveX == 0 && MoveY == 0) break;
        if (NewX == 0 && NewY == 0) break;
    }
}

void CEntity::StopMove()
{
    SpeedX = 0;
    SpeedY = 0;

    AccelX = 0;
    AccelY = 0;
}

bool CEntity::Collides(int oX, int oY, int oW, int oH)
{
    int left1, left2;
    int right1, right2;
    int top1, top2;
    int bottom1, bottom2;

    int tX = (int)X + Col_X;
    int tY = (int)Y + Col_Y;

    left1 = tX;
    left2 = oX;

    right1 = left1 + Width - 1 - Col_Width;
    right2 = oY + oH -1;

    top1 = tY;
    top2 = oY;

    bottom1 = top1 + Height - 1 - Col_Height;
    bottom2 = oY + oH -1;

    if (bottom1 < top2) return false;
    if (top1 > bottom2) return false;

    if (right1 < left2) return false;
    if (left1 > right2) return false;

    return true;
}

CEntity* CEntity::EntityInArea(int oX, int oY, int oW, int oH)
{
    for (int i=0; i < EntityList.size(); i++)
    {
        if (EntityList[i]->Collides(oX, oY, oW, oH) == true)
        {
            return EntityList[i];
        }
    }
    return nullptr;
}

bool CEntity::PosValid(int NewX, int NewY)
{
    bool Return = true;

    int StartX = (NewX + Col_X) / TILE_WIDTH;
    int StartY = (NewY + Col_Y) / TILE_HEIGHT;

    int EndX = ((NewX + Col_X) + Width - Col_Width - 1) / TILE_WIDTH;
    int EndY = ((NewY + Col_Y) + Height - Col_Height - 1) / TILE_HEIGHT;

    for (int iY=StartY; iY <= EndY; iY++)
    {
        for (int iX=StartX; iX <= EndX; iX++)
        {
            CTile* Tile = CArea::AreaControl.GetTile(iX * TILE_WIDTH, iY * TILE_HEIGHT);

            if (PosValidTile(Tile) == false) Return = false;
        }
    }

    if (Flags & ENTITY_FLAG_MAPONLY)
    {
        // Do nothing
    }
    else
    {
        for (int i=0; i < EntityList.size(); i++)
        {
            if (PosValidEntity(EntityList[i], NewX, NewY) == false)
            {
                Return = false;
            }
        }
    }

    return Return;
}

bool CEntity::PosValidTile(CTile* Tile)
{
    if (Tile == nullptr) return true;

    if (Tile->TypeID == TILE_TYPE_BLOCK) return false;

    return true;
}

bool CEntity::PosValidEntity(CEntity* Entity, int NewX, int NewY)
{
    if (this != Entity && 
        Entity != nullptr && 
        Entity->Dead == false && 
        Entity->Flags ^ ENTITY_FLAG_MAPONLY &&
        Entity->Collides(NewX + Col_X, NewY + Col_Y, Width - Col_Width -1, Height - Col_Height - 1) == true)
    {
        CEntityCol EntityCol;

        EntityCol.EntityA = this;
        EntityCol.EntityB = Entity;

        CEntityCol::EntityColList.push_back(EntityCol);

        return false;
    }

    return true;
}

bool CEntity::OnInteract()
{
    std::cout << "(CEntity) Spacebar has been pressed" << std::endl;
    return true;
}