#ifndef _CFPS_H_
#define _CFPS_H_

#include <SDL2/SDL.h>

class CFPS
{
private:
    int OldTime;
    int LastTime;
    int NumFrames;
    int Frames;

    float SpeedFactor;

public:
    static CFPS FPSControl;

public:
    CFPS();

    int GetFPS();
    float GetSpeedFactor();

    void OnLoop();
};

#endif