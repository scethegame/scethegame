CXX = g++

SDL_LIB = -lSDL2 -lSDL2_image
SDL_INCLUDE = -I"/usr/include/SDL2"

CXXFLAGS = -c -std=c++11
EXE = demo
RELEASE_DIR = release

SRC_FILES = src/main.cpp src/CApp.cpp src/CTile.cpp src/CMap.cpp src/CTexture.cpp src/CArea.cpp src/CCamera.cpp src/CFPS.cpp src/CAnimation.cpp src/CEntityCol.cpp src/CEntity.cpp src/CPlayer.cpp src/CEvent.cpp
COMPILED_FILES = CTile.o CMap.o CTexture.o CArea.o CCamera.o CApp.o CFPS.o CAnimation.o CEntity.o CEntityCol.o CPlayer.o CEvent.o main.o

all: clean build

build:
	$(CXX) $(CXXFLAGS) $(SDL_INCLUDE) -g -c $(SRC_FILES);
	$(CXX) $(COMPILED_FILES) $(SDL_LIB) -o $(RELEASE_DIR)/$(EXE);

clean:
	rm *.o && rm $(RELEASE_DIR)/$(EXE);
